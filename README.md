# Dte Api

# 后端API接口框架

公司项目后端API接口使用Hyperf2开发，且在组件生态完善下保持Hyperf最新版本；

## 项目概述

* 产品名称：地摊儿商城
* 项目代号：dte

## 运行环境要求

- PHP 7.2+
- Swoole PHP 扩展 >= 4.5，并关闭了 Short Name
- OpenSSL PHP 扩展
- JSON PHP 扩展
- PDO PHP 扩展 （如需要使用到 MySQL 客户端）
- Redis PHP 扩展 （如需要使用到 Redis 客户端）
- Mysql 5.7+
- Redis 5.0+

## 服务器架构说明

![架构](https://cdn.learnku.com/uploads/images/201705/20/1/1G6aQPAZym.png)

## 开发环境安装

团队成员本地开发环境**推荐**使用 [Laravel Homestead](https://learnku.com/docs/laravel/5.8/homestead)。

考虑公司机器性能问题以及部分同事开发习惯，使用本地安装开发环境只是为次级选择和快速使用选择

### 安装前置

如果还没有安装 Composer，在 Linux 和 Mac OS X 中可以运行如下命令：
~~~
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
~~~
在 Windows 中，你需要下载并运行 Composer-Setup.exe。

由于众所周知的原因，国外的网站连接速度很慢。因此安装的时间可能会比较长，我们建议通过下面的方式使用国内镜像。

打开命令行窗口（windows用户）或控制台（Linux、Mac 用户）并执行如下命令：
~~~
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
~~~

### 配置Homestead
~~~
folders:
    - map: ~/path/dte/ # 你本地的项目目录地址
      to: /home/vagrant/dte

databases:
    - dte
~~~

使用`vagrant`启动`homestead`

~~~
vagrant up
~~~

### 安装Swoole
~~~
sudo pecl install swoole
~~~

### 安装Redis
~~~
sudo pecl install redis
~~~


### 升级composer
~~~
sudo composer self-update
~~~

### 创建项目
~~~
composer create-project dte/skeleton dte-test
~~~

### 安装依赖
~~~
composer install
~~~

### 初次安装
~~~
php -r "file_exists('.env') || copy('.env.example', '.env');"
~~~

修改`.env`文件数据库和`redis`相关配置
运行数据库迁移
~~~
php vendor/bin/hope-bootstrap migrate:refresh --seed
~~~

### 启动服务
~~~
composer watch
~~~

然后就可以在浏览器中访问

~~~
http://192.168.10.10:9501/liveness
~~~

看到`ok`表示安装成功

### 更新框架
~~~
composer update hyperf
~~~

### 升级框架

按照官方文档指引完成升级

框架始终保持官方LTS版本（大改动除外）

## 在线手册

+ [官方在线文档](https://hyperf.wiki/2.0)

## 目录结构

目录结构如下：

~~~
部署目录（或者子目录）
├─app                    应用目录
│  ├─Command             命令目录
│  ├─Constants           常量定义
│  ├─Cron                定时任务
│  ├─Event               事件目录
│  ├─Http                调用第三方平台接口
│  ├─Job                 消息队列，临时
│  ├─Listener            事件监听者目录
│  ├─Model               模型目录 
│  ├─Request             请求验证目录
│  ├─Rpc                 远程调用目录
│  ├─Api.php             API控制器分层
│  ├─ErrorCode.php       错误码
│  ├─Grpc.php            grpc服务层
│  ├─Service.php         API服务层
│  └─Utils.php           工具类
|
├─config                  配置文件目录，可选
├─migrations              数据结构迁移目录
├─storage                 资源目录
├─test                    单元测试目录
├─vendor                  依赖包目录
│
├─.env                    环境配置文件
├─.env.example            环境配置文件模板
├─composer.json           composer 定义文件
├─Dockerfile              Docker配置文件
├─phpunit.xml             单元测试配置文件
├─README.md               README 文件
~~~
