<?php

declare(strict_types=1);

namespace App;

use App\Request\TestRequest;
use App\Rpc\TestGrpc;
use Dte\Common\Model\UserAccountModel;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use NanQi\Hope\Base\BaseController;

/**
 * @Controller()
 * Class controller
 */
class Api extends BaseController {

    /**
     * @var Service
     */
    private $service;

    /**
     * Controller constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @GetMapping(path="/test")
     */
    public function test(TestRequest $testRequest)
    {
        $name = $testRequest->inputString('name');
        $name = $this->service->test($name) . '1123';
        return $name;
    }
}