<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use NanQi\Hope\Base\BaseConstants;

/**
 * @Constants
 */
class TestConstants extends BaseConstants
{
    /**
     * @Message("测试")
     */
    const TEST = 'test';
}
