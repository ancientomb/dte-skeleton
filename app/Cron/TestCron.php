<?php

declare(strict_types=1);

namespace App\Cron;

use NanQi\Hope\Annotation\Cron;
use NanQi\Hope\Base\BaseCron;

/**
 * Class TestCron 1234
 * @Cron(rule="* * * * * *")
 * @package App\Cron
 */
class TestCron extends BaseCron
{
    /**
     * cron执行入口
     * @return mixed
     * @throws \Throwable
     */
    public function execute()
    {
        $this->getLog()->info(mt_rand(1, 100000));
    }
}
