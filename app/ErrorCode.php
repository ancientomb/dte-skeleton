<?php

declare(strict_types=1);

namespace App;

use Hyperf\Constants\Annotation\Constants;
use NanQi\Hope\Base\BaseConstants;

/**
 * @Constants
 */
class ErrorCode extends BaseConstants
{
    //region 用户类 1XXX
    /**
     * @Message("账号密码错误")
     */
    const USER_PWD_ERROR_1001 = 1001;
    //endregion
}
