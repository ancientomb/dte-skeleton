<?php

namespace App;

use NanQi\Hope\Ext\FixInterface;
use NanQi\Hope\Helper;

class Fix implements FixInterface
{
    use Helper;

    public function test($id)
    {
        info('id:' . $id);
    }
}