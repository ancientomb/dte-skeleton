<?php

declare(strict_types=1);

namespace App;

use Dte\TestRequest;
use Dte\TestResponse;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\PostMapping;
use NanQi\Hope\Base\BaseController;

/**
 * @Controller(server="grpc")
 * Class controller
 */
class Grpc extends BaseController {

    /**
     * @PostMapping(path="/test")
     */
    public function test(TestRequest $testRequest) : TestResponse
    {
        $name = $testRequest->getName();
        $res = new TestResponse();
        $res->setMessage('hello ' . $name);
        return $res;
    }
}