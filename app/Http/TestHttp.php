<?php

declare(strict_types=1);

namespace App\Http;

use Hyperf\CircuitBreaker\Annotation\CircuitBreaker;
use NanQi\Hope\Annotation\Hystrix;
use NanQi\Hope\Base\BaseHttp;
use Psr\Http\Message\ResponseInterface;

/**
 * Class TestHttp
 * @package App\Rpc
 */
class TestHttp extends BaseHttp
{
    /**
     * @Hystrix()
     * @param $sleep
     * @return ResponseInterface
     */
    protected function test($sleep)
    {
        return $this->client->get('http://go-api.k8s.bjjieao.top/test?sleep=' . $sleep);
    }
}