<?php

declare(strict_types=1);

namespace App\Job;
use NanQi\Hope\Base\BaseJob;

class TestJob extends BaseJob {

    public function __construct(int $val)
    {
        $this->val = $val;
    }

    /**
     * Handle the job.
     */
    public function handle()
    {
    }
}