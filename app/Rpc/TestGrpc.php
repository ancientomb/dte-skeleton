<?php

declare(strict_types=1);

namespace App\Rpc;

use Dte\TestRequest;
use Dte\TestResponse;
use NanQi\Hope\Annotation\Grpc;

/**
 * @Grpc(svc="127.0.0.1:9504")
 * Class TestGrpc 7
 * @package App\Rpc
 */
class TestGrpc
{
    public function test(TestRequest $testRequest) : TestResponse
    {
    }
}