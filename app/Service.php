<?php

declare(strict_types=1);

namespace App;

use NanQi\Hope\Base\BaseService;

class Service extends BaseService {

    public function test(string $name) : string
    {
        return $name . ' hello';
    }
}