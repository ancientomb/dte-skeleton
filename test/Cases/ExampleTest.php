<?php

declare(strict_types=1);

namespace Test\Cases;

use App\Service;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class ExampleTest extends TestCase
{
    public function testExample()
    {
        /** @var Service $service */
        $service = di(Service::class);

        $res = $service->test('nanqi');

        $this->assertEquals('nanqi hello', $res);
    }
}
