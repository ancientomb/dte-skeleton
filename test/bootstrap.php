<?php

declare(strict_types=1);

use NanQi\Hope\Factory\AppFactory;

! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));
require_once BASE_PATH . '/vendor/autoload.php';

AppFactory::createApp();